package likelion.hospitalreview.service;

import likelion.hospitalreview.domain.User;
import likelion.hospitalreview.exception.AppException;
import likelion.hospitalreview.exception.ErrorCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import likelion.hospitalreview.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public String join(String userName, String password) {

        // userName 중복 체크
        userRepository.findByUserName(userName)
                .ifPresent(user ->
                {
                    throw new AppException(ErrorCode.USERNAME_DUPLICATED, "[" + userName + "]" + "이 이미 존재합니다.");
                });

        // 중복 체크 후 저장
        User user = User.builder()
                .userName(userName)
                .password(password)
                .build();
        userRepository.save(user);

        return "SUCCESS";
    }
}